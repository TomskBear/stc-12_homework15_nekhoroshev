package ru.innopolis.stc12.jdbc.realExample.dao;

import ru.innopolis.stc12.jdbc.realExample.dao.city.CityDao;
import ru.innopolis.stc12.jdbc.realExample.dao.city.CityDaoImpl;
import ru.innopolis.stc12.jdbc.realExample.dao.student.StudentDao;
import ru.innopolis.stc12.jdbc.realExample.dao.student.StudentDaoImpl;

public class DaoManager {
    StudentDao studentDao;
    CityDao cityDao;

    public DaoManager() {
        studentDao = new StudentDaoImpl(this);
        cityDao = new CityDaoImpl(this);
    }

    public StudentDao getStudentDao() {
        return studentDao;
    }

    public CityDao getCityDao() {
        return cityDao;
    }
}
