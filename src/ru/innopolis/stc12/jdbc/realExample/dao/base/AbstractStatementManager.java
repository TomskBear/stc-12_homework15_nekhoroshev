package ru.innopolis.stc12.jdbc.realExample.dao.base;

import ru.innopolis.stc12.jdbc.realExample.connectionManager.ConnectionManager;
import ru.innopolis.stc12.jdbc.realExample.connectionManager.ConnectionManagerJdbcImpl;
import ru.innopolis.stc12.jdbc.realExample.dao.DaoManager;

import java.sql.Connection;

public abstract class AbstractStatementManager {
    private static ConnectionManager connectionManager = ConnectionManagerJdbcImpl.getInstance();
    private DaoManager daoManager;

    protected AbstractStatementManager(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    protected Connection getConnection(){
        return connectionManager.getConnection();
    }

    protected DaoManager getDaoManager(){
        return daoManager;
    }
}
