package ru.innopolis.stc12.jdbc.realExample.dao.city;
import ru.innopolis.stc12.jdbc.realExample.pojo.City;
public interface CityDao {
    public boolean addCity(City city);

    public boolean deleteCityById(int id);

    public boolean updateCity(City city);

    public City getCityById(int id);
}
