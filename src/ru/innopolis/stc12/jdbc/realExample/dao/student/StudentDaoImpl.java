package ru.innopolis.stc12.jdbc.realExample.dao.student;

import ru.innopolis.stc12.jdbc.realExample.dao.DaoManager;
import ru.innopolis.stc12.jdbc.realExample.dao.base.AbstractDao;
import ru.innopolis.stc12.jdbc.realExample.pojo.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentDaoImpl extends AbstractDao implements StudentDao {
        private StudentStatementManager studentStatementManager;

    public StudentDaoImpl(DaoManager manager) {
        super(manager);
        studentStatementManager = new StudentStatementManager(manager);
    }

    @Override
    public boolean addStudent(Student student) {
        try{
            return tryExecuteStatement(studentStatementManager.getInsertStudentStatement(student));
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Student getStudentById(int id) {
        Student student = null;

        try (ResultSet resultSet = executeStatement(studentStatementManager.getStudentByIdStatement(id))){
            if (resultSet.next()) {
                student = studentStatementManager.mapResultSetToStudent(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return student;
    }

    @Override
    public boolean update(Student student) {
        if (student.getId() != 0) {
            try {
                return tryExecuteStatement(studentStatementManager.getUpdateStudentStatement(student));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteStudentById(int id) {
        try {
            return tryExecuteStatement(studentStatementManager.getDeleteStudentByIdStatement(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteStudentByName(Student student) {
        try {
            return tryExecuteStatement(studentStatementManager.getDeleteStudentByNameStatement(student.getName()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
