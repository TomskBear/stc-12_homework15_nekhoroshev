package ru.innopolis.stc12.jdbc.realExample;

import ru.innopolis.stc12.jdbc.realExample.dao.DaoManager;
import ru.innopolis.stc12.jdbc.realExample.dao.city.CityDao;
import ru.innopolis.stc12.jdbc.realExample.dao.student.StudentDao;
import ru.innopolis.stc12.jdbc.realExample.pojo.Student;

public class Main {
    public static void main(String[] args) {
        DaoManager daoManager = new DaoManager();
        CityDao cityDao = daoManager.getCityDao();
        cityDao.deleteCityById(3);
    }
}
